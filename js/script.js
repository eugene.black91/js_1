// 1. Огласить переменную можно с помощью ключевых слов: let/const
// 2. Prompt запрашивает информацию у пользователя; confirm просит подтвердить у пользователня в диалоговом окне с помощью двух кнопок ок и отмена
// 3. Так как JavaScript — это язык со слабой типизацией, значения могут быть конвертированы между различными типами автоматически. Это называют неявным приведением типов.
// Например: 1 == null, 2/’5'

let admin;
let userName = "Eugene";
admin = userName;

console.log(admin);

let days = 8;
console.log(days * 86400);

let lastName = prompt("What's your last name?");
console.log(lastName);
